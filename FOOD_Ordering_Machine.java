import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FOOD_Ordering_Machine {
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextArea textArea1;
    private JPanel root;

    private JLabel Total;

    private JButton checkOutButton;
    private JTextArea textArea2;
    private  int total = 0;
    private  double price = 0;


    public static void main(String[] args) {
        JFrame frame = new JFrame("GUI");
        frame.setContentPane(new FOOD_Ordering_Machine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public void order(String food,int cost) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            int x = JOptionPane.showConfirmDialog(null,
                    "Would you like eat inside ?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if(x == 0) {
                price = cost * 1.08;
                total += price;
                textArea1.append("(I)" + food + " " + cost + "yen\n");
                textArea2.append("(I)" + food + " " + (int)Math.floor(price) + "yen\n");
                JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

            }
            else {
                int y = JOptionPane.showConfirmDialog(null,
                        "Would you like eat takeout ?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (y == 0) {
                    price = cost * 1.1;
                    total += price;
                    textArea1.append("(T)" + food + " " + cost + "yen\n");
                    textArea2.append("(T)" + food + " " + (int)Math.floor(price) + "yen\n");
                    JOptionPane.showMessageDialog(null, "Order for " + food + " received.");

                }
            }
            Total.setText("Total    " + total + "yen");

        }
    }
public FOOD_Ordering_Machine() {
            button1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    order("Thunder soba",950);
                }
            });
            button1.setIcon(new ImageIcon(
                    this.getClass().getResource("Img2093.jpg")
            ));

    button2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            order("Ramen",1300);
        }
    });
    button2.setIcon(new ImageIcon(
            this.getClass().getResource("Imgramen2.jpg")
    ));
    button3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            order("Tsukemen",1500);
        }
    });
    button3.setIcon(new ImageIcon(
            this.getClass().getResource("Imgtuke3.jpg")
    ));
    button6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            order("Topping",1200);
        }
    });
    button6.setIcon(new ImageIcon(
            this.getClass().getResource("Imgtamago.jpg")
    ));
    button5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            order("Chinese soba(salt)",900);
        }
    });
    button5.setIcon(new ImageIcon(
            this.getClass().getResource("tomita_shokudo_img005.jpg")
    ));
    button4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            order("Rich Tsukemen",920);
        }
    });
    button4.setIcon(new ImageIcon(
            this.getClass().getResource("kizuna1.jpg")
    ));
    checkOutButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to check out ? ",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {
                textArea1.setText("");
                textArea2.setText("");
                JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen");
                Total.setText("Total" );

            }
        }
    });


}


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}

